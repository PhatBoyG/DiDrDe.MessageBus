﻿using Autofac;
using DiDrDe.Contracts;
using DiDrDe.MessageBus.IoCC.Autofac;
using DiDrDe.Model;
using System;

namespace DiDrDe.ConsumerOne
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ConfigureIoCc();
            Console.Title = "Consumer One";
            Console.WriteLine("Awaiting event messages on Consumer One");
            Console.ReadLine();
        }

        private static IContainer ConfigureIoCc()
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterType<ThingHappenedHandler>()
                .As<IEventDtoHandler<ThingHappened>>()
                .InstancePerDependency();

            builder
                .RegisterAssemblyTypes(typeof(ThingHappenedHandler).Assembly)
                .AssignableTo<IEventDtoHandler>()
                .AsSelf()
                .As<IEventDtoHandler>();

            builder.RegisterMessageBusConsumer(options =>
            {
                options.WithQueueName("DiDrDe.ConsumerOne");
            });

            var container = builder.Build();
            return container;
        }
    }
}
