﻿using DiDrDe.Contracts;
using DiDrDe.Model;
using System;
using System.Threading.Tasks;

namespace DiDrDe.ConsumerOne
{
    public class ThingHappenedHandler
        : IEventDtoHandler<ThingHappened>
    {
        public Task HandleAsync(ThingHappened eventDto)
        {
            Console.WriteLine($"Received {eventDto.Name} " +
                                $"{eventDto.Description} at consumer one that uses an IEventDtoHandler");
            return Task.CompletedTask;
        }
    }
}
