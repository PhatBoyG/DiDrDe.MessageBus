﻿using System;
using System.Threading.Tasks;
using DiDrDe.Model;
using MassTransit;

namespace DiDrDe.ConsumerTwo
{
    public class AnotherThingHappenedConsumer
        : IConsumer<ThingHappened>

    {
        public Task Consume(ConsumeContext<ThingHappened> context)
        {
            Console.WriteLine($"Received {context.Message.Name} " +
                              $"{context.Message.Description} at consumer two");
            return Task.CompletedTask;
        }
    }
}