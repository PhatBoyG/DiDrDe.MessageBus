﻿using Autofac;
using MassTransit;
using System;

namespace DiDrDe.ConsumerTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ConfigureIoCc();
            Console.Title = "Consumer Two";
            Console.ReadKey();
        }

        private static IContainer ConfigureIoCc()
        {
            var builder = new ContainerBuilder();

            builder.Register(context =>
                {
                    var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        var host = cfg.Host(new Uri("rabbitmq://localhost/"), h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });

                        cfg.ReceiveEndpoint(host, "DiDrDe.ConsumerTwo", consumer =>
                        {
                            consumer.Consumer<AnotherThingHappenedConsumer>();
                        });
                    });
                    return busControl;
                })
                .As<IBusControl>()
                .As<IBus>()
                .SingleInstance();

            builder
                .RegisterType<ReceiverBus>()
                .As<IStartable>()
                .SingleInstance();

            var container = builder.Build();
            return container;
        }
    }
}
