﻿using System;
using Autofac;
using MassTransit;
using MassTransit.Util;

namespace DiDrDe.ConsumerTwo
{
    public class ReceiverBus
        : IStartable, IDisposable
    {
        private readonly IBusControl _busControl;

        public ReceiverBus(IBusControl busControl)
        {
            _busControl = busControl;

        }

        public void Dispose()
        {
            TaskUtil.Await(() => _busControl.StopAsync());
        }

        public void Start()
        {
            TaskUtil.Await(() => _busControl.StartAsync());
        }
    }
}