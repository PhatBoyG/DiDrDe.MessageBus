﻿using System.Threading.Tasks;

namespace DiDrDe.Contracts
{
    public interface IEventDtoBus
    {
        Task PublishAsync<TEventDto>(TEventDto eventDto) where TEventDto : class, IEventDto;
    }
}