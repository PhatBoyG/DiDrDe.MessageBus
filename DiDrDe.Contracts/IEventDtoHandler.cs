﻿using System.Threading.Tasks;

namespace DiDrDe.Contracts
{
    public interface IEventDtoHandler
    {
    }

    public interface IEventDtoHandler<in TEventDto>
        : IEventDtoHandler
            where TEventDto : IEventDto
    {
        Task HandleAsync(TEventDto eventDto);
    }
}