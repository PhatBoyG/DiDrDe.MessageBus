﻿using DiDrDe.Contracts;
using MassTransit;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public class EventDtoBus
        : IEventDtoBus
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public EventDtoBus(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task PublishAsync<TEventDto>(TEventDto eventDto) where TEventDto : class, IEventDto
        {
            await _publishEndpoint.Publish(eventDto);
        }
    }
}