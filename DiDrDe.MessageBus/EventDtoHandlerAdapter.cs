﻿using DiDrDe.Contracts;
using MassTransit;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public class EventDtoHandlerAdapter<TEventDto>
        : IConsumer<TEventDto> where TEventDto : class, IEventDto
    {
        private readonly IEventDtoHandler<TEventDto> _consumer;

        public EventDtoHandlerAdapter(IEventDtoHandler<TEventDto> consumer)
        {
            _consumer = consumer;
        }

        public async Task Consume(ConsumeContext<TEventDto> context)
        {
            await _consumer.HandleAsync(context.Message);
        }
    }
}