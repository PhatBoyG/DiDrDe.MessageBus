﻿using Autofac;
using DiDrDe.Contracts;
using DiDrDe.Model;
using MassTransit;
using System;
using System.Linq;
using GreenPipes.Internals.Extensions;

namespace DiDrDe.MessageBus.IoCC.Autofac
{
    public static class RegistrationExtensions
    {
        public static void RegisterMessageBusPublisher(this ContainerBuilder builder,
            Action<MessageBusOptions> configureOptions = null)
        {
            var messageBusOptions = new MessageBusOptions();
            configureOptions?.Invoke(messageBusOptions);

            builder.Register(context =>
                {
                    var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        var host = cfg.Host(new Uri("rabbitmq://localhost/"), h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });

                        cfg.OverrideDefaultBusEndpointQueueName("PublisherQueue");
                    });
                    return busControl;
                })
                .As<IBusControl>()
                .As<IBus>()
                .As<IPublishEndpoint>()
                .SingleInstance();

            builder
                .RegisterType<EventDtoBus>()
                .As<IEventDtoBus>()
                .InstancePerDependency();

            builder
                .RegisterType<PublisherBus>()
                .As<IStartable>()
                .SingleInstance();
        }

        public static void RegisterMessageBusConsumer(this ContainerBuilder builder,
            Action<MessageBusOptions> configureOptions = null)
        {
            var messageBusOptions = new MessageBusOptions();
            configureOptions?.Invoke(messageBusOptions);

            var handlerTypes =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(x => x.GetTypes())
                    .Where(x => typeof(IEventDtoHandler).IsAssignableFrom(x)
                                && !x.IsInterface
                                && !x.IsAbstract)
                    .ToList();

            foreach (var handlerType in handlerTypes)
            {
                var eventType = handlerType.GetClosingArguments(typeof(IEventDtoHandler<>)).Single();
                var interfaceType = typeof(IEventDtoHandler<>).MakeGenericType(eventType);
                var adapterType = typeof(EventDtoHandlerAdapter<>).MakeGenericType(eventType);

                builder.RegisterType(handlerType).As(interfaceType).InstancePerLifetimeScope();
                builder.RegisterType(adapterType).InstancePerLifetimeScope();
            }

            builder.Register(context =>
                {
                    var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        var host = cfg.Host(new Uri("rabbitmq://localhost/"), h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });

                        cfg.ReceiveEndpoint(host, messageBusOptions.QueueName, consumer =>
                        {
                            consumer.Consumer<EventDtoHandlerAdapter<ThingHappened>>(context);
                        });
                    });
                    return busControl;
                })
                .As<IBusControl>()
                .As<IBus>()
                .SingleInstance();

            builder
                .RegisterType<EventDtoBus>()
                .As<IEventDtoBus>()
                .InstancePerDependency();

            builder
                .RegisterType<PublisherBus>()
                .As<IStartable>()
                .SingleInstance();
        }
    }
}