﻿using System;
using System.Collections.ObjectModel;

namespace DiDrDe.MessageBus.IoCC
{
    public class MessageBusOptions
    {
        public Uri ServerUri { get; private set; } = new Uri("rabbitmq://localhost");
        public string Username { get; private set; } = "guest";
        public string Password { get; private set; } = "guest";
        public string QueueName { get; private set; }

        public MessageBusOptions WithRabbitMqUri(Uri serverUri)
        {
            ServerUri = serverUri;
            return this;
        }

        public MessageBusOptions WithUsername(string username)
        {
            Username = username;
            return this;
        }

        public MessageBusOptions WithPassword(string password)
        {
            Password = password;
            return this;
        }

        public MessageBusOptions WithQueueName(string queueName)
        {
            QueueName = queueName;
            return this;
        }
    }
}