﻿using System;
using Autofac;
using MassTransit;
using MassTransit.Util;

namespace DiDrDe.MessageBus
{
    public class PublisherBus
        : IStartable, IDisposable
    {
        private readonly IBusControl _busControl;

        public PublisherBus(IBusControl busControl)
        {
            _busControl = busControl;

        }

        public void Dispose()
        {
            TaskUtil.Await(() => _busControl.StopAsync());
        }

        public void Start()
        {
            TaskUtil.Await(() => _busControl.StartAsync());
        }
    }
}