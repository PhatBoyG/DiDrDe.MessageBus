﻿using DiDrDe.Contracts;

namespace DiDrDe.Model
{
    public class ThingHappened
        : IEventDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}