﻿using Autofac;
using DiDrDe.Contracts;
using DiDrDe.MessageBus.IoCC.Autofac;
using DiDrDe.Model;
using System;

namespace DiDrDe.Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ConfigureIoCc();
            Console.Title = "Publisher";
            Console.WriteLine("Type message to publish");
            var publisher = container.Resolve<IEventDtoBus>();
            const int count = 5;
            var messagesSent = 0;
            while (messagesSent < count)
            {
                var message = Console.ReadLine();

                Publish(publisher, message);
                Console.WriteLine("Message published");
                messagesSent++;
            }
            Console.In.ReadLine();
            Console.WriteLine("END");
        }

        private static void Publish(IEventDtoBus publisher, string message)
        {
            var thingHappened =
                new ThingHappened
                {
                    Name = message,
                    Description = $"Event sent on {DateTime.UtcNow.ToShortTimeString()}"
                };
            publisher.PublishAsync(thingHappened).GetAwaiter().GetResult();
        }

        private static IContainer ConfigureIoCc()
        {
            var builder = new ContainerBuilder();

            builder.RegisterMessageBusPublisher();

            var container = builder.Build();
            return container;
        }
    }
}