# DiDrDe.MessageBus

Project created for issue: https://stackoverflow.com/questions/52805079/how-to-register-a-generic-consumer-adapter-in-masstransit-if-i-have-a-list-of-me

Requirements:
- Install and run RabbitMq locally (it needs Erlang installed)

How to Test:
- In a bash console
- `cd DiDrDe.Publisher`
- `dotnet run`
- In another bash console
- `cd DiDrDe.ConsumerOne`
- `dotnet run`
- In another bash console
- `cd DiDrDe.ConsumerTwo`
- `dotnet run`